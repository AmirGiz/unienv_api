require File.expand_path('lib/unienv_api/version', __dir__)

Gem::Specification.new do |spec|
  spec.name                  = 'unienv_api'
  spec.version               = UnienvApi::VERSION
  spec.authors               = ['Gizatullin Amirkhan']
  spec.email                 = ['amiranto@mail.ru']
  spec.summary               = 'UniEnvApi gem'
  spec.description           = 'Library for working with API UniEnv'
  spec.homepage              = 'https://gitlab.com/AmirGiz/unienv_api'
  spec.license               = 'MIT'
  spec.platform              = Gem::Platform::RUBY
  spec.required_ruby_version = '>= 2.7.2'

  spec.files = Dir['README.md', 'LICENSE',
                   'CHANGELOG.md', 'lib/**/*.rb',
                   'lib/**/*.rake', 'lib/**/*.md',
                   'lokalise_rails.gemspec', '.gitignore',
                   'Gemfile', 'Rakefile']

  spec.extra_rdoc_files = ['README.md']

  spec.add_dependency 'oauth2', '~> 1.4.7'
  spec.add_runtime_dependency 'typhoeus', '~> 1.0', '>= 1.0.1'
  spec.add_runtime_dependency 'json', '~> 2.1', '>= 2.1.0'
  spec.add_runtime_dependency 'addressable', '~> 2.3', '>= 2.3.0'

  spec.add_development_dependency 'rspec', '~> 3.6', '>= 3.6.0'
  spec.add_development_dependency 'vcr', '~> 3.0', '>= 3.0.1'
  spec.add_development_dependency 'webmock', '~> 1.24', '>= 1.24.3'
  spec.add_development_dependency 'autotest', '~> 4.4', '>= 4.4.6'
  spec.add_development_dependency 'autotest-rails-pure', '~> 4.1', '>= 4.1.2'
  spec.add_development_dependency 'autotest-growl', '~> 0.2', '>= 0.2.16'
  spec.add_development_dependency 'autotest-fsevent', '~> 0.2', '>= 0.2.12'
end