# unienv_api

## Быстрый старт
**Гем UniEnv** - устанавливаемый из пакетного менеджера модуль, который позволяет взаимодействовать с платформой UniEnv: производить авторизацию пользователей через OAuth и выполнять API запросы.


```ruby 
        require 'unienv_api'
        
        # Авторизация
        @unienv = UniEnv.new(client_id, client_secret, redirect_uri)
        
        # получение ссылки
        auth_url = @unienv.authorize
        
        # получение токена
        token = @unienv.get_token(code)
        
        # отзыв токена
        re_token = @unienv.revoke_token
        
        # получение нового токена
        new_token = auth.refresh_token
        
        # API        
        # получение профиля
        profile = @unienv.get_profile
```


## Авторизация
Реализованы следующие методы:
+ `authorize` - возвращает ссылку для переадресации пользователя на страницу авторизации
+ `get_token(code)` - получение access token c дополнительными данными для выполнения следующих запросов
+ `revoke_token`  - отзыв токена (выход)
+ `refresh_token` - обновление токена

## API
Реализованы методы для обращения к API UniEnv
Методы сгенерировались на основе документации в формате OpenAPI (Swagger):
+ `get_profile` - получение профиля