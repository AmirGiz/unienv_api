require 'oauth2'
require 'swagger_client'

class UniEnv
  # Инициализация инстанса класса
  # :param client_id: client id
  # :param client_secret: client secret
  # :param redirect_uri: адрес, на который пользователь будет возвращен после авторизации
  def initialize(client_id, client_secret, redirect_uri)
    @unienv = OAuth2::Client.new(client_id, client_secret, { site: 'https://core.uenv.ru', redirect_uri: redirect_uri} )
  end

  # Возвращает ссылку для переадресации пользователя на страницу авторизации
  def authorize
    @unienv.auth_code.authorize_url
  end

  # Получение access token c дополнительными данными для выполнения следующих запросов
  # :param code: код авторизации
  def get_token(code)
    @token = @unienv.auth_code.get_token(code)
    @token.token
  end

  # Отзыв токена
  def revoke_token
    header = { 'Content-Type' => 'application/x-www-form-urlencoded' }
    body = { token: @token.token, client_id: @unienv.id, client_secret: @unienv.secret }
    @response = @unienv.request(:post, 'https://uenv-core.kpfu.ru/oauth/revoke_token/', {header: header, body: body})
    @response.status == 200? 'Токен отозван' : "Не удалось отозвать токен - #{@response.response_body.to_s}"
  end

  # Обновление токена
  def refresh_token
    @token.refresh!.token
  end

  # Получение профиля
  def get_profile
    SwaggerClient.configure.access_token = @token.token
    @api = SwaggerClient::ProfileApi.new
    @api.profile_get
  end
end
