# SwaggerClient::ProfileApi

All URIs are relative to *http://uenv-core.kpfu.ru/api/v1.0/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**profile_get**](ProfileApi.md#profile_get) | **GET** /profile/ | Получение профиля


# **profile_get**
> Profile profile_get(opts)

Получение профиля



### Example
```ruby
# load the gem
require 'swagger_client'
# setup authorization
SwaggerClient.configure do |config|
  # Configure HTTP basic authorization: Basic
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = SwaggerClient::ProfileApi.new

opts = { 
  limit: 56, # Integer | Number of results to return per page.
  offset: 56 # Integer | The initial index from which to return the results.
}

begin
  #Получение профиля
  result = api_instance.profile_get(opts)
  p result
rescue SwaggerClient::ApiError => e
  puts "Exception when calling ProfileApi->profile_get: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **limit** | **Integer**| Number of results to return per page. | [optional] 
 **offset** | **Integer**| The initial index from which to return the results. | [optional] 

### Return type

[**Profile**](Profile.md)

### Authorization

[Basic](../README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json



