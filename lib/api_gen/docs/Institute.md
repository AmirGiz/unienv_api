# SwaggerClient::Institute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**title** | **String** |  | 
**short_title** | **String** |  | [optional] 


