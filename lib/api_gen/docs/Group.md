# SwaggerClient::Group

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**title** | **String** |  | 
**start_year** | **Integer** |  | [optional] 
**end_year** | **Integer** |  | [optional] 
**current_course** | **String** | Номер курса | [optional] 


