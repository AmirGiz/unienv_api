# SwaggerClient::Profile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**email** | **String** |  | [optional] 
**first_name** | **String** |  | 
**last_name** | **String** |  | 
**middle_name** | **String** |  | [optional] 
**role** | **String** |  | [optional] 
**student_group** | [**Group**](Group.md) |  | [optional] 
**institute** | [**Institute**](Institute.md) |  | 
**photo_url** | **String** |  | [optional] 
**gitlab_auth_time** | **String** | Авторизация в Gitlab: дата последней авторизации или null, если авторизации еще не было | [optional] 


